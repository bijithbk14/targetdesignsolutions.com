-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: mirapools
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint unsigned DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint unsigned DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject` (`subject_type`,`subject_id`),
  KEY `causer` (`causer_type`,`causer_id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'default','App\\Models\\Page model has been created','App\\Models\\Page',5,'App\\Models\\User',1,'[]','2022-03-03 08:25:52','2022-03-03 08:25:52'),(2,'default','App\\Models\\Page model has been created','App\\Models\\Page',6,'App\\Models\\User',1,'[]','2022-03-03 08:41:40','2022-03-03 08:41:40'),(3,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',1,'App\\Models\\User',1,'[]','2022-03-03 08:43:56','2022-03-03 08:43:56'),(4,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',2,'App\\Models\\User',1,'[]','2022-03-03 09:25:52','2022-03-03 09:25:52'),(5,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',3,'App\\Models\\User',1,'[]','2022-03-03 09:29:28','2022-03-03 09:29:28'),(6,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',4,'App\\Models\\User',1,'[]','2022-03-03 09:30:45','2022-03-03 09:30:45'),(7,'default','App\\Models\\Service model has been created','App\\Models\\Service',1,'App\\Models\\User',1,'[]','2022-03-03 09:52:59','2022-03-03 09:52:59'),(8,'default','App\\Models\\Service model has been deleted','App\\Models\\Service',1,'App\\Models\\User',1,'[]','2022-03-03 09:54:44','2022-03-03 09:54:44'),(9,'default','App\\Models\\Service model has been created','App\\Models\\Service',2,'App\\Models\\User',1,'[]','2022-03-03 09:55:15','2022-03-03 09:55:15'),(10,'default','App\\Models\\Service model has been created','App\\Models\\Service',3,'App\\Models\\User',1,'[]','2022-03-03 10:19:56','2022-03-03 10:19:56'),(11,'default','App\\Models\\Service model has been created','App\\Models\\Service',4,'App\\Models\\User',1,'[]','2022-03-03 10:21:11','2022-03-03 10:21:11'),(12,'default','App\\Models\\Service model has been created','App\\Models\\Service',5,'App\\Models\\User',1,'[]','2022-03-03 10:22:14','2022-03-03 10:22:14'),(13,'default','App\\Models\\Service model has been created','App\\Models\\Service',6,'App\\Models\\User',1,'[]','2022-03-03 10:23:20','2022-03-03 10:23:20'),(14,'default','App\\Models\\Service model has been created','App\\Models\\Service',7,'App\\Models\\User',1,'[]','2022-03-03 10:24:11','2022-03-03 10:24:11'),(15,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',5,'App\\Models\\User',1,'[]','2022-03-03 10:28:03','2022-03-03 10:28:03'),(16,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',6,'App\\Models\\User',1,'[]','2022-03-03 10:33:24','2022-03-03 10:33:24'),(17,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',7,'App\\Models\\User',1,'[]','2022-03-03 10:35:39','2022-03-03 10:35:39'),(18,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',8,'App\\Models\\User',1,'[]','2022-03-03 10:36:37','2022-03-03 10:36:37'),(19,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',9,'App\\Models\\User',1,'[]','2022-03-03 10:38:02','2022-03-03 10:38:02'),(20,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',10,'App\\Models\\User',1,'[]','2022-03-03 10:39:16','2022-03-03 10:39:16'),(21,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',11,'App\\Models\\User',1,'[]','2022-03-03 10:40:49','2022-03-03 10:40:49'),(22,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',12,'App\\Models\\User',1,'[]','2022-03-03 10:41:57','2022-03-03 10:41:57'),(23,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',13,'App\\Models\\User',1,'[]','2022-03-03 10:43:58','2022-03-03 10:43:58'),(24,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',14,'App\\Models\\User',1,'[]','2022-03-03 10:44:48','2022-03-03 10:44:48'),(25,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',15,'App\\Models\\User',1,'[]','2022-03-03 10:45:46','2022-03-03 10:45:46'),(26,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',16,'App\\Models\\User',1,'[]','2022-03-03 10:48:29','2022-03-03 10:48:29'),(27,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',17,'App\\Models\\User',1,'[]','2022-03-03 10:49:44','2022-03-03 10:49:44'),(28,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',18,'App\\Models\\User',1,'[]','2022-03-03 10:50:33','2022-03-03 10:50:33'),(29,'default','App\\Models\\Category model has been created','App\\Models\\Category',1,'App\\Models\\User',1,'[]','2022-03-04 22:31:48','2022-03-04 22:31:48'),(30,'default','App\\Models\\Category model has been created','App\\Models\\Category',2,'App\\Models\\User',1,'[]','2022-03-04 22:31:55','2022-03-04 22:31:55'),(31,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',19,'App\\Models\\User',1,'[]','2022-03-04 22:38:46','2022-03-04 22:38:46'),(32,'default','App\\Models\\Gallery model has been deleted','App\\Models\\Gallery',19,'App\\Models\\User',1,'[]','2022-03-04 23:28:42','2022-03-04 23:28:42'),(33,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',20,'App\\Models\\User',1,'[]','2022-03-05 04:58:35','2022-03-05 04:58:35'),(34,'default','App\\Models\\ServiceDetail model has been created','App\\Models\\ServiceDetail',1,'App\\Models\\User',1,'[]','2022-03-05 21:27:38','2022-03-05 21:27:38'),(35,'default','App\\Models\\ServiceDetail model has been deleted','App\\Models\\ServiceDetail',1,'App\\Models\\User',1,'[]','2022-03-05 22:00:47','2022-03-05 22:00:47'),(36,'default','App\\Models\\Gallery model has been deleted','App\\Models\\Gallery',20,'App\\Models\\User',1,'[]','2022-03-05 22:12:17','2022-03-05 22:12:17'),(37,'default','App\\Models\\Testimonial model has been created','App\\Models\\Testimonial',1,'App\\Models\\User',1,'[]','2022-03-09 21:16:41','2022-03-09 21:16:41'),(38,'default','App\\Models\\Testimonial model has been created','App\\Models\\Testimonial',2,'App\\Models\\User',1,'[]','2022-03-09 21:17:01','2022-03-09 21:17:01'),(39,'default','App\\Models\\Testimonial model has been created','App\\Models\\Testimonial',3,'App\\Models\\User',1,'[]','2022-03-09 21:17:18','2022-03-09 21:17:18'),(40,'default','App\\Models\\Testimonial model has been updated','App\\Models\\Testimonial',3,'App\\Models\\User',1,'[]','2022-03-09 21:49:12','2022-03-09 21:49:12');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'2022-03-04 22:31:48','2022-03-04 22:31:48','Completed Projects'),(2,'2022-03-04 22:31:55','2022-03-04 22:31:55','Ongoing Projects');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactuses`
--

DROP TABLE IF EXISTS `contactuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contactuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactuses`
--

LOCK TABLES `contactuses` WRITE;
/*!40000 ALTER TABLE `contactuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `contactuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galleries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_FK` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'2022-03-03 08:43:56','2022-03-03 08:43:56','Residential Swimming pool','https://res.cloudinary.com/dxxlsebas/image/upload/v1646316835/njaioxhxiepb9bn5e6s0.jpg','Residential Swimming pool',1),(2,'2022-03-03 09:25:52','2022-03-03 09:25:52','Swimming Pool','https://res.cloudinary.com/dxxlsebas/image/upload/v1646319351/oca9rhlowylqdyfn34fd.jpg','Swimming Pool',1),(3,'2022-03-03 09:29:28','2022-03-03 09:29:28','Fish Pond','https://res.cloudinary.com/dxxlsebas/image/upload/v1646319568/q2qncxge7egfwftsre6a.jpg','Fish Pond',1),(4,'2022-03-03 09:30:45','2022-03-03 09:30:45','Fountain','https://res.cloudinary.com/dxxlsebas/image/upload/v1646319644/cvknutuawaoqhackzksm.jpg','Fountain',1),(5,'2022-03-03 10:28:03','2022-03-03 10:28:03','water features','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323083/nmd2jgk3kumel5nrwdot.jpg','wathe features',1),(6,'2022-03-03 10:33:24','2022-03-03 10:33:24','mohanlal residence','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323404/tyv0q7vkbhvdkaxpzfmf.jpg','mohanlal residence',1),(7,'2022-03-03 10:35:39','2022-03-03 10:35:39','Swimming Pool','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323539/eyqdwk2nsxesspdbpfpj.jpg','Swimming Pool',1),(8,'2022-03-03 10:36:37','2022-03-03 10:36:37','Fish fond','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323597/gjvbaonwc42l6kkzdclh.jpg','Fish Pond',1),(9,'2022-03-03 10:38:02','2022-03-03 10:38:02','Fountain','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323682/uj44ftvjsrgfdtkefbkc.jpg','Fountain',1),(10,'2022-03-03 10:39:16','2022-03-03 10:39:16','Fountain','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323755/lbjoyxvcfw6rulsbgg0q.jpg','Fountain',1),(11,'2022-03-03 10:40:49','2022-03-03 10:40:49','water Feature','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323849/odxy8smn7epaoosskgoc.jpg','Water Feature',1),(12,'2022-03-03 10:41:57','2022-03-03 10:41:57','Fountain','https://res.cloudinary.com/dxxlsebas/image/upload/v1646323915/zd3htqm78cshyldrklen.jpg','Fountain',1),(13,'2022-03-03 10:43:58','2022-03-03 10:43:58','Swimming Pool','https://res.cloudinary.com/dxxlsebas/image/upload/v1646324037/famws07lxfk3hvoz25tz.jpg','Swimming pool',1),(14,'2022-03-03 10:44:48','2022-03-03 10:44:48','Fauntain','https://res.cloudinary.com/dxxlsebas/image/upload/v1646324088/nfyc61jhbdzsacrcv7vm.jpg','Fauntain',1),(15,'2022-03-03 10:45:46','2022-03-03 10:45:46','Swimming Pool','https://res.cloudinary.com/dxxlsebas/image/upload/v1646324145/awini67gulxx33gft3ec.jpg','Swimming Pool',1),(16,'2022-03-03 10:48:29','2022-03-03 10:48:29','Water Feature','https://res.cloudinary.com/dxxlsebas/image/upload/v1646324308/f0jtfpbndxcqfvybavp6.jpg','Water Feature',1),(17,'2022-03-03 10:49:44','2022-03-03 10:49:44','Swimming Pool','https://res.cloudinary.com/dxxlsebas/image/upload/v1646324383/vwu0haxwkp8rswqytdib.jpg','Swimming Pool',1),(18,'2022-03-03 10:50:33','2022-03-03 10:50:33','Fountain','https://res.cloudinary.com/dxxlsebas/image/upload/v1646324432/ou88acpwphgvgye1tz2p.jpg','Fountain',1);
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_193651_create_roles_permissions_tables',1),(4,'2018_08_04_122319_create_settings_table',1),(5,'2019_08_19_000000_create_failed_jobs_table',1),(6,'2019_12_14_000001_create_personal_access_tokens_table',1),(7,'2021_12_10_121031_create_activity_log_table',1),(8,'2021_12_31_042153_create_pages_table',1),(9,'2022_01_08_122629_create_galleries_table',1),(10,'2022_01_24_114242_create_contactuses_table',1),(11,'2022_03_03_151143_create_services_table',2),(12,'2022_03_03_151636_create_services_table',3),(13,'2022_03_05_035317_create_categories_table',4),(14,'2022_03_06_015415_create_service_details_table',5),(15,'2022_03_10_024554_create_testimonials_table',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template` int DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_edit` int DEFAULT NULL,
  `order` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,NULL,NULL,1,'Home | Mira Water Management','Mira water management systems, is a water management company from cochin Kerala to serve the entire nation. We have special attention in the field of swimming pool construction and water treatment service.',NULL,'Home','home.blade.php',NULL,'home',1,1),(2,NULL,NULL,1,'About Us | Mira Water Management','Mira water management systems, is a water management company from cochin Kerala to serve the entire nation. We have special attention in the field of swimming pool construction and water treatment service.',NULL,'About US','about-us.blade.php',NULL,'about-us',1,2),(3,NULL,NULL,1,'Product & Services | Mira Water Management','Mira water management systems, is a water management company from cochin Kerala to serve the entire nation. We have special attention in the field of swimming pool construction and water treatment service.',NULL,'Product & Services','services.blade.php',NULL,'services',0,3),(4,NULL,NULL,1,'Contact Us | Mira Water Management','Mira water management systems, is a water management company from cochin Kerala to serve the entire nation. We have special attention in the field of swimming pool construction and water treatment service.',NULL,'Contact Us','contact-us.blade.php',NULL,'contact-us',1,6),(5,'2022-03-03 08:25:52','2022-03-03 08:25:52',1,'Features | Mira Water Management','Mira water management systems, is a water management company from cochin Kerala to serve the entire nation. We have special attention in the field of swimming pool construction and water treatment service.','Mira water management','Features','features.blade.php',NULL,'features',1,4),(6,'2022-03-03 08:41:40','2022-03-03 08:41:40',1,'Gallery | Mira Water Management','Mira water management systems, is a water management company from cochin Kerala to serve the entire nation. We have special attention in the field of swimming pool construction and water treatment service.','mira pools','Gallery','gallery.blade.php',NULL,'gallery',0,5);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user` (
  `role_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_details`
--

DROP TABLE IF EXISTS `service_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_details`
--

LOCK TABLES `service_details` WRITE;
/*!40000 ALTER TABLE `service_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (2,'2022-03-03 09:55:15','2022-03-03 09:55:15','SWIMMING POOL & SPA','Mira is one of the best swimming pool and spa developers in Kerala. Because we only use quality products of international brands. Quality makes the name Mira Water Management Systems an outstanding service provider among the crowd. Our swimming pool accessories and spa equipment meet all international standards.','https://res.cloudinary.com/dxxlsebas/image/upload/v1646321114/duimac7iaqdymzrseb6z.jpg'),(3,'2022-03-03 10:19:56','2022-03-03 10:19:56','FOUNTAINS & WATER FEATURES','One of our main services is designing fountains and water features. We make several types of fountains which include finger jet fountains, umbrella nozzles, form jet nozzles, fan jet nozzles, glass curtain fountains, wall curtain fountains, water cascade and water sheets. Portable fountain kits. Submersible underwater lights.','https://res.cloudinary.com/dxxlsebas/image/upload/v1646322595/jg2ts8yo1wxwlvknxxyt.jpg'),(4,'2022-03-03 10:21:11','2022-03-03 10:21:11','STEAM & SAUNA EQUIPMENTS','We take care of procurement and installation of steam and sauna equipment for our clients. Various factors are to be considered to get optimum efficiency from these machines. We also assist our client with the design of the room where the equipment is to be placed. Our experts guide you through the entire process to get the best results.','https://res.cloudinary.com/dxxlsebas/image/upload/v1646322670/srmheqgsj206xygeuk6p.jpg'),(5,'2022-03-03 10:22:14','2022-03-03 10:22:14','WATER TREATMENT PLANT','We install and commission all kinds of water treatment plants for drinking water treatment. We have our labs to test the water and to ensure the best quality. We use techniques like pressure sand filtration, carbon filtration, water softening, iron removal, reverse osmosis, micron filtration, automatic chemical dosing to help treat water.','https://res.cloudinary.com/dxxlsebas/image/upload/v1646322733/qfeqwgpuxlezj0zrewzt.jpg'),(6,'2022-03-03 10:23:20','2022-03-03 10:23:20','SEWAGE/ EFFLUENT TREATMENT PLANT','We take-up the fitting and commissioning of wastewater/ effluent treatment plants. We have our labs to test the water and to ensure the best quality treatment of the effluents. We use various physical, chemical, and biological techniques to treat these effluents. Our experts design each treatment plant, based on the effluent discharge.','https://res.cloudinary.com/dxxlsebas/image/upload/v1646322799/f1xhjgrnojzdsoweqsqp.jpg'),(7,'2022-03-03 10:24:11','2022-03-03 10:24:11','UV WATER PURIFIERS & ULTRA FILTRATION SYSTEMS','We provide UV Water Purifiers & Ultra-Filtration Systems for our clients based on their specific requirements. Our team of experts takes care of every step involved in the installation of these systems. Also, we undertake mineral water plants installation and commissioning.','https://res.cloudinary.com/dxxlsebas/image/upload/v1646322850/oevsmy4uo8chy8tn2lqr.jpg');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonials` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial` text COLLATE utf8mb4_unicode_ci,
  `who_is_who` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'2022-03-09 21:16:41','2022-03-09 21:16:41','Nuhan','I want to say a big thank you to MIRA for all the help they\'ve given us. Whenever we have a problem, they are available and ready to offer their excellent customer service and get back to us right away. They have shown themselves as the utmost professionals, and we always appreciate that when doing business with them.',NULL),(2,'2022-03-09 21:17:01','2022-03-09 21:17:01','Mashood','I have been using MIRA for a few months now and I couldn\'t be happier with the service! The customer service is excellent and they really care about your business. They are responsive, timely and honest.',NULL),(3,'2022-03-09 21:17:18','2022-03-09 21:49:12','Sojan','I had a great experience with MIRA. I cannot say enough about their customer service and the level of accountability that they have demonstrated every time we work together. I hope to keep doing business with them in the future and will recommend them to my colleagues.','CEO');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Mirapools','info@mirapools.com',NULL,'$2y$10$7dI6.ZfljtF7TvIYCM4ymeyLEXyU4xGXGYK62Ad/4gvjah7VNBX2m',NULL,'2022-03-03 01:40:37','2022-03-03 01:40:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-10  9:17:39
