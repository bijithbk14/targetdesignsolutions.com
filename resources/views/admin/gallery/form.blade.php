<div class="form-group{{ $errors->has('category') ? 'has-error' : '' }}">
    {!! Form::label('category', 'Category', ['class' => 'control-label']) !!}
    <select name="category" class="form-control" id="category" required>
        <option value="">--Select Category--</option>
            @foreach ($category as $item)
                <option value="{{ $item->id }}" @if (isset($gallery) && $item->id == $gallery->category) selected @endif>{{ $item->category }}</option>
            @endforeach
    </select>
    {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image') ? 'has-error' : '' }}">
    {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
    @if (isset($gallery) && $gallery->image != '')
        <div>
            <img onerror="this.onerror=null;this.src='https://res.cloudinary.com/dxxlsebas/image/upload/v1639911822/error_rwgafw.png';" src="{{ $gallery->image }}" width="50" height="50">
            <a href="{{ route('remove_gallery_image') }}?id={{ $gallery->id }}"
                onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger">delete</a>
        </div>
    @else
        {!! Form::file('image', 'required' == 'required' ? ['class' => 'form-control', 'required' => 'required', 'accept' => 'image/*'] : ['class' => 'form-control', 'accept' => 'image/*']) !!}
    @endif
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
