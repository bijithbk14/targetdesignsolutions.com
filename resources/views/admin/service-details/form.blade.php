<div class="form-group{{ $errors->has('service') ? 'has-error' : '' }}">
    {!! Form::label('Service', 'Service', ['class' => 'control-label']) !!}
    <select name="service" class="form-control" id="service" required>
        <option value="">--Select Service--</option>
            @foreach ($services as $item)
                <option value="{{ $item->id }}" @if (isset($servicedetail) && $item->id == $servicedetail->service) selected @endif>{{ $item->title }}</option>
            @endforeach
    </select>
    {!! $errors->first('service', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Type', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
{{-- <div class="form-group{{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div> --}}
<div class="form-group{{ $errors->has('image') ? 'has-error' : '' }}">
    {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
    @if (isset($servicedetail) && $servicedetail->image != '')
        <div>
            <img onerror="this.onerror=null;this.src='https://res.cloudinary.com/dxxlsebas/image/upload/v1639911822/error_rwgafw.png';" src="{{ $servicedetail->image }}" width="50" height="50">
            <a href="{{ route('remove_service_details_image') }}?id={{ $servicedetail->id }}"
                onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger">delete</a>
        </div>
    @else
        {!! Form::file('image', 'required' == 'required' ? ['class' => 'form-control', 'required' => 'required', 'accept' => 'image/*'] : ['class' => 'form-control', 'accept' => 'image/*']) !!}
    @endif
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
