@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Create New Test</h6>
            </div>
            <div class="card-body">
                 <a href="{{ url('/admin/test') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                 <br><br>
                 @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                  {!! Form::open(['url' => '/admin/test', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin.test.form', ['formMode' => 'create'])

                        {!! Form::close() !!}
        </div>
        </div>
        </div>
@endsection
