<div class="form-group{{ $errors->has('test') ? 'has-error' : ''}}">
    {!! Form::label('test', 'Test', ['class' => 'control-label']) !!}
    {!! Form::text('test', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('test', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
