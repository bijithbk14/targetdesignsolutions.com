@extends('frontend.base')
@section('content')

<div class="banner inner-page service-banner">
    <div class="banner-content">
        <div class="container">
            <div class="row">
                <div class="banner-text">
                    <h1 class="page-title">{{$service->title}}</h1>
                    <p class="page-breadcrumb">Home / <span class="current text-capitalize" autocomplete="off">{{$service->title}}</span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End banner -->
@if (count($details)>0)

<section class="portfolio section tz-gallery" data-vvveb-disabled="">
    <div class="container">
        <div class="filters">
            <ul>
                <li class="active" data-filter="*">All</li>
                @foreach ($gallery_category as $item)
                    <li data-filter=".{{ \Str::slug($item) }}">{{ $item }}</li>
                @endforeach
            </ul>
        </div>

        <div class="filters-content">
            <div class="row grid">
                @foreach ($details as $item)
                    <div class="col-sm-4 all {{\Str::slug($item->title)}}">
                        <div class="item">
                            <a class="lightbox" href="{{ $item->image }}">
                            <img src="{{ $item->image }}" alt="{{ $item->title }}">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
</section>
@else
<div class="container mt-40 mb-40">
    <h2>No Data</h2>
</div>
@endif
<!-- Main Content -->

<!-- End Main content -->
@section('scripts')
    <script>
        $('.filters ul li').click(function() {
            $('.filters ul li').removeClass('active');
            $(this).addClass('active');

            var data = $(this).attr('data-filter');
            $grid.isotope({
                filter: data
            })
        });

        var $grid = $(".grid").isotope({
            itemSelector: ".all",
            percentPosition: true,
            masonry: {
                columnWidth: ".all"
            }
        })
    </script>
@endsection
@endsection
