@extends('frontend.base')
@section('content')
<!-- Banner -->
  <div class="banner inner-page feature-banner">
    <div class="banner-content">
        <div class="container">
            <div class="row">
                <div class="banner-text">
                    <h1 class="page-title">Features</h1>
                    <p class="page-breadcrumb">Home / <span class="current" autocomplete="off">Features</span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End banner -->
<div class="main-content ser-page section-pad">
    <div class="container">
        <div class="row">
            <div class="service-des-holder row">
                <ul class="service-wrapper clearfix icon-list-over">
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="#">
                                <h4>EXPERIENCED TECHNICIANS</h4>
                                <p>
                                    We employ a team of qualified experts to help our clients through their unique requirements and likes and design a very customized pool or plant. Our team has years of experience in the domain and is trained to understand our client's needs.
                                </p>
                            </a>
                        </div>
                    </li>
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="#">
                                <h4>UNIQUE DESIGN</h4>
                                <p>
                                    We constantly strive to provide a solution or design that matches your brand as an individual or an organization. Our design specialists try and deliver unparalleled designs which match the landscape and aesthetics of the place.
                                </p>
                            </a>
                        </div>
                    </li>
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="#">
                                <h4>TURNKEY PROJECTS</h4>
                                <p>We undertake all works from scratch, whether it is a swimming pool or a water treatment plant. We are a team of skilled professionals who have the experience to handle a project from the initial planning stage till final execution.</p>
                            </a>
                        </div>
                    </li>
                </ul>
                <ul class="service-wrapper clearfix icon-list-over">
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="service-single.html">
                                <h4>QUALITY PRODUCTS</h4>
                                <p>We deal with numerous quality products of internationally reputed brands for our clients. We make it a point to procure the best quality products and ensure spare availability of these products, if and when required.</p>
                            </a>
                        </div>
                    </li>
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="service-single.html">
                                <h4>ON-TIME COMPLETION</h4>
                                <p>
                                    Our team of experts works round the clock to ensure that all the projects we undertake are commissioned within the stipulated time and handed over to our clients. Delivering quality service on time every time is our mantra.
                                </p>
                            </a>
                        </div>
                    </li>
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="service-single.html">
                                <h4>BEST AFTER-SALE SERVICE</h4>
                                <p>
                                    We never consider delivery of a service as end our involvement with our clients. Our team ensures to provide the best after-sales support to each of our clients in case of any post-commissioning requirements.
                                </p>
                            </a>
                        </div>
                    </li>
                </ul>
                <ul class="service-wrapper clearfix icon-list-over">

                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="service-single.html">
                                <h4>OPERATION& MAINTENANCE CONTRACT</h4>
                                <p>After service is very important for swimming pools and water treatment units. We provide you a permanent operator at your location with a very reasonable payment to make your swimming pool or spa hygienic all the time. This will be guaranteed by an annual maintenance contract agreement.</p>
                            </a>
                        </div>
                    </li>
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="service-single.html">
                                <h4>LABORATORY ANALYSIS</h4>
                                <p>We have our laboratory with a team of expert technicians at cochin to test your water quality. We broadly consider three kinds of testing to check the quality of the water: 1) Physical 2) Chemical 3)Bacteriological. We add up the required minerals and treat the water to make it usable.</p>
                            </a>
                        </div>
                    </li>
                    <li class="col-sm-4 pad-xs-0 clearfix">
                        <div class="ser-col">
                            <a href="service-single.html">
                                <h4>STRUCTURAL & DESIGN CONSULTANCY</h4>
                                <p>We estimate requirements to design your swimming pool based on your budget. We suggest ideas, plans and also provide design consultancy. We have creative in-house designers who help you design pools. You only need to share your ideas or ask them for ideas for the best and unique designs.</p>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
















<div id="gtx-trans" style="position: absolute; left: 65px; top: 187.188px;"><div class="gtx-trans-icon"></div></div>
@section('scripts')

@endsection
@endsection
