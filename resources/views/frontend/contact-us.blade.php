@extends('frontend.base')
@section('content')
    <div class="breadcrumb-area breadcrumb-padding bg-img" style="background-image:url(/frontend/assets/images/bg/bg-2.jpg)">
        <div class="container">
            <div class="breadcrumb-content text-center">
                <h2>Contact Us</h2>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>
    </div>
     <!-- Project area -->
     <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <div class="contact-info-wrap">
                        <div class="single-contact-info-wrap">
                            <div class="info-icon">
                                <i class="dlicon ui-1_home-51"></i>
                            </div>
                            <div class="info-content">
                                <h3 class="title">Address</h3>
                                <p class="width">
                                    36/546-k, 2 nd floor, manath ayyath building, manath ayyath moola road, near chithranjali studio, kakkanad- 682021
                                </p>
                            </div>
                        </div>
                        <div class="single-contact-info-wrap">
                            <div class="info-icon">
                                <i class="dlicon ui-2_phone"></i>
                            </div>
                            <div class="info-content">
                                <h3 class="title">Phone</h3>
                                <p> Mobile: <span>+91 9895482013</span></p>
                                <p> Mobile: <span>+91 6238426257</span></p>
                            </div>
                        </div>
                        <div class="single-contact-info-wrap">
                            <div class="info-icon">
                                <i class="dlicon ui-1_email-85"></i>
                            </div>
                            <div class="info-content">
                                <h3 class="title">Email</h3>
                                <p>info@targetdesignsolutions.com</p>
                            </div>
                        </div>
                        <div class="single-contact-info-wrap">
                            <div class="info-icon">
                                <i class="dlicon ui-2_share"></i>
                            </div>
                            <div class="info-content">
                                <h3 class="title">Follow us</h3>
                                <div class="social-icon-style mt-4">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-whatsapp"></i></a>
                                    <a class="google-plus" href="#"><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6">
                    <div class="contact-from-wrap">
                        <form id="contact-form" action="/send-enquery" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input name="name" type="text" placeholder="Name" required>
                            <input name="email" type="email" placeholder="Email" required>
                            <input name="phone" type="phone" placeholder="Phone" required>
                            <input name="subject" type="hidden" value="Enquery" placeholder="Subject" required>
                            <textarea name="message" placeholder="Your message" required></textarea>
                            <input class="submit" type="submit" value="Send Message">
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
            </div>
            {{-- <div class="map mt-6 mt-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31431.405896811568!2d76.31857403955078!3d10.022987100000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080d0eb648122d%3A0xd98b98db4974ba60!2sMIRA%20WATER%20MANAGEMENT%20PVT.LTD.(MIRA%20POOLS)!5e0!3m2!1sen!2sin!4v1646314816738!5m2!1sen!2sin"></iframe>
            </div> --}}
        </div>
    </div>
@endsection
