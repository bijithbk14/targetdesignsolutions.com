@extends('frontend.base')
@section('content')
    <!-- slider area -->
    <div class="hero-area">
        <div class="hero-slider-active swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="intro-section height-100vh slider-content-center bg-img single-animation-wrap slider-bg-color-1 overly-style-1 opacity-point-4"
                        style="background-image:url(/frontend/assets/images/slider/slider-1.jpg)">
                        <div class="container hover_plx">
                            <div class="hero-content-1 slider-animated-1 text-center layer" data-depth="-0.7">
                                <h1 class="title animated">TARGET DESIGN SOLUTIONS</h1>
                                <p class="animated">We're always happy to go the extra mile and make your dreams come true.
                                </p>
                                <a href="/pages/gallery" class="btn btn-primary btn-hover-dark animated">VIEW GALLERY</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="intro-section height-100vh slider-content-center bg-img single-animation-wrap slider-bg-color-1 overly-style-1 opacity-point-4"
                        style="background-image:url(/frontend/assets/images/slider/slider-2.jpg)">
                        <div class="container hover_plx">
                            <div class="hero-content-1 slider-animated-1 text-center layer" data-depth="-0.7">
                                <h1 class="title animated">TARGET DESIGN SOLUTIONS</h1>
                                <p class="animated">We're always happy to go the extra mile and make your dreams come true.
                                </p>
                                <a href="/pages/gallery" class="btn btn-primary btn-hover-dark animated">VIEW GALLERY</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home-slider-prev main-slider-nav"><i class="fa fa-angle-left"></i></div>
            <div class="home-slider-next main-slider-nav"><i class="fa fa-angle-right"></i></div>
        </div>
    </div>
    <!-- About area -->
    <div class="section section-padding">
        <div class="container">
            <div class="row row-cols-lg-2 row-cols-1 mb-n6">
                <div class="col align-self-center mb-6">
                    <div class="about-content">
                        <h2 class="title">Welcome to <br><span>Target design solutions</span></h2>
                        <p>We are passionate, knowing that people live, work, play and inhabit the spaces and places we
                            imagine and envision for our clients. To those wanting to make an impact with design.</p>

                    </div>
                    <div class="working-away-wrap">
                        <div class="working-away">
                            <div class="working-away-icon">
                                <i class="dlicon ui-1_home-51"></i>
                            </div>
                            <div class="working-content">
                                <h3 class="title">Qualified Planning</h3>
                                <p>Your interior and exterior projects deserve more than just your average planning. A
                                    qualified interior and exterior design partner is available to help you move forward
                                    quickly and confidently towards completion.</p>
                            </div>
                        </div>
                        <div class="working-away">
                            <div class="working-away-icon">
                                <i class="dlicon design_measure-big"></i>
                            </div>
                            <div class="working-content">
                                <h3 class="title">Professional Design</h3>
                                <p>
                                    Our designers work with you on the design of your project ensuring you get the best
                                    finished product. This includes audits, 3D modeling and specialty finishes to take your
                                    design to the next level.
                                </p>
                            </div>
                        </div>
                    </div>
                    <a href="/pages/about-us" class="btn btn-dark btn-hover-primary">LEARN MORE</a>
                </div>
                <div class="col mb-6">
                    <div class="about-banner">
                        <img src="/frontend/assets/images/banner/banner-2.png" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services area -->
    <div class="section section-padding pt-0">
        <div class="container">
            <div class="section-title text-center mb-lg-9 mb-md-7 mb-5">
                <h2 class="title">Our <span>Services</span></h2>
                <p>
                    Our design consultants enjoy the freedom of creativity and the specialized moxie to take your design
                    from a dream to a reality. Clients often turn to us for help in finding the perfect mix of old-world
                    luxury and new-world cool.
                </p>
            </div>
            <div class="row  mb-n6">
                <div class="col-md-6 mb-6">
                    <div class="service text-center">
                        <img src="/frontend/assets/images/service/interior.jpg" alt="banner">
                        <div class="service-content-wrap">
                            <div class="service-content">
                                <br>
                                <div class="service-icon">
                                    <i class="dlicon furniture_light"></i>
                                </div>
                                <h3 class="title"><a href="#">Interior Design</a></h3>
                                <p>
                                    Target Design Solutions is a multi-service interior design firm setting its own bar out
                                    of the ordinary. Whether you're a business looking to revitalize a space or you're a
                                    homeowner in need of a renovation, our design service seeks to maximize functionality
                                    and perfection. Our skilled team strives to exceed expectations and our signature will
                                    help your space become the best it can be.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-6">
                    <div class="service text-center">
                        <img src="/frontend/assets/images/service/exterior.jpg" alt="banner">
                        <div class="service-content-wrap">
                            <div class="service-content">
                                <br>
                                <div class="service-icon">
                                    <i class="dlicon design_design"></i>
                                </div>
                                <h3 class="title"><a href="#">Exterior Design</a></h3>
                                <p>Our team is experienced in this field and has taken on several notable projects. We
                                    understand it's extra stressful when a deadline looms and you're unsure as to how it
                                    will turn out. For this reason, we stand by our word and our work with a 100%
                                    satisfaction guarantee. If you're not happy, we're not happy.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Working Process area -->
    <div class="section section-padding bg-img overly-style-1 opacity-point-7"
        style="background-image:url(/frontend/assets/images/bg/bg-1.jpg)">
        <div class="container">
            <div class="section-title text-center mb-lg-9 mb-md-7 mb-5">
                <h2 class="title white">Our Working<span> Process</span></h2>
                <p class="white">We're always willing to go the redundant afar, to make your dream home a reality, through
                    a perfect mix of creativity and specialized moxie. </p>
            </div>
            <div class="row row-cols-lg-4 row-cols-md-2 row-cols-sm-2 row-cols-1 mb-n6">
                <div class="col mb-6">
                    <div class="working-process text-center">
                        <i class="dlicon users-2_b-meeting"></i>
                        <h4 class="title">Meet The Client</h4>
                    </div>
                </div>
                <div class="col mb-6">
                    <div class="working-process text-center">
                        <i class="dlicon business_bulb-62"></i>
                        <h4 class="title">Make Ideas</h4>
                    </div>
                </div>
                <div class="col mb-6">
                    <div class="working-process text-center">
                        <i class="dlicon business_hierarchy-54"></i>
                        <h4 class="title">Development</h4>
                    </div>
                </div>
                <div class="col mb-6">
                    <div class="working-process text-center">
                        <i class="dlicon business_building"></i>
                        <h4 class="title">Test & Release</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Project area -->
    <div class="section section-padding">
        <div class="container">
            <div class="section-title text-center mb-lg-8 mb-md-6 mb-4">
                <h2 class="title">Our <span> Gallery</span></h2>
                <p>We love to showcase the work we have completed as we're always extremly proud of what we can and do
                    achieve. Please browse through out recent gallery and let us know what you think!</p>
            </div>
            <div class="project-menu-style project-menu-active isotope-btn-add-active mb-6">
                <button class="active btn-project" data-filter="*">All</button>
                <button class="btn-project" data-filter=".interior">Interior</button>
                <button class="btn-project" data-filter=".exterior">Exterior</button>
            </div>
            <div class="row row-cols-lg-3 row-cols-md-2 row-cols-sm-2 row-cols-1 mb-n6 grid">
                @foreach ($gallery as $item)
                <div class="col mb-6 grid-item {{\Str::slug($item->category_data->category)}}">
                    <div class="project-wrap">
                        <div class="project-inner">
                            <a class="thumbnail" href="{{ $item->image }}">
                                <img src="{{ $item->image }}" alt="{{ $item->title }}">
                            </a>
                            <div class="project-content">
                                <span class="category"><a href="#">{{$item->category_data->category}}</a></span>
                                <h3 class="title"><a href="{{ $item->image }}">{{ $item->description }}</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="project-btn mt-6 mt-md-8 mt-lg-10 text-center">
                <a href="/pages/gallery" class="btn btn-dark btn-hover-primary">VIEW MORE</a>
            </div>
        </div>
    </div>
    <!-- Video area -->
    <div class="section video-banner">
        <div class="banner-content">
            <video preload="none" loop="loop" poster="/frontend/assets/images/bg/bg-3.jpg">
                <source src="/frontend/assets/video/video.mp4" type="video/mp4">
            </video>
            <div class="status play-pause-style">
                <i class="fa fa-play"></i>
            </div>
        </div>
    </div>
@section('scripts')
@endsection
@endsection
