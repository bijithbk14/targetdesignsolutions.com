@extends('frontend.base')
@section('content')
    <div class="breadcrumb-area breadcrumb-padding bg-img" style="background-image:url(/frontend/assets/images/bg/bg-2.jpg)">
        <div class="container">
            <div class="breadcrumb-content text-center">
                <h2>About Us</h2>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>About Us</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- About area -->
    <div class="section section-padding">
        <div class="container">
            <div class="row row-cols-lg-2 row-cols-1 mb-n6">
                <div class="col align-self-center mb-6">
                    <div class="about-content">
                        <h2 class="title">Welcome to <br><span>Target design solutions</span></h2>
                        <p>We are passionate, knowing that people live, work, play and inhabit the spaces and places we imagine and envision for our clients. To those wanting to make an impact with design.</p>
                        <p>
                            It's time to wind up your hunt for ultra ultramodern infrastructures, contemporary infrastructures, new ultra modern home designs, 3d architectural designs, new house bottom plans, geography armature, house addition, erecting designs, structural design & innards by hiring us. Target Design Solutions is a colonist in leading-edge architectural & interior designing services with stylish leading engineers and interior contrivers in Kochi (Cochin), Ernakulam, Kerala. Our armature establishment with further than 20 times of experience in different disciplines, we conceptualize your dreams and help you fantasize your dream space in virtual form, to get your suggestions before initiating the factual process. We unleash our creativity to give the perfect shape to the dream home of each one of you; guaranteeing oneness in all that we do, in a budget-friendly yet sustainable manner.
                        </p>
                    </div>
                </div>
                <div class="col mb-6">
                    <div class="about-banner">
                        <img src="/frontend/assets/images/banner/banner-2.png" alt="banner">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class=" section-padding">
        <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="section-title-1">
                <h4 class="title">Our <br><span>Mission</span></h4>
                </div>
                <p>
                    We are always willing to go the extra mile, to make your dream home a reality, through a perfect blend of creativity and technical expertise.
                </p>
            </div>
            <div class="col-md-3">
                <div class="section-title-1">
                <h4 class="title">Our <br><span>Vission</span></h4>
                </div>
                <p>
                    To rise above the expectations of our clients by rendering unbeatable services, and thereby establish ourselves as a leading name in the domain.
                </p>
            </div>
            <div class="col-md-3">
                <div class="section-title-1">
                <h4 class="title">Exclusive <br><span>Designs</span></h4>
                </div>
                <p>
                    With our help, you can design, create, and build the most wonderful place in the world.
                </p>
            </div>
            <div class="col-md-3">
                <div class="section-title-1">
                <h4 class="title">Wide Range <br><span>Varities</span></h4>
                </div>
                <p>
                    Our aim is for good designs to offer a wide variety of different experiences and attract all kinds of people who believe in talent.
                </p>
            </div>
        </div>
        </div>
    </div>
@endsection
