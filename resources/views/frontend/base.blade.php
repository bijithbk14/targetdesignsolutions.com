
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$page->meta_title ? $page->meta_title : 'Mira Water Management'}}</title>
    <meta name="description" content="{{$page->meta_description ? $page->meta_description : 'Mira Water Management'}}">
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/assets/images/favicon.png')}}">

    <!-- CSS (Font, Vendor, Icon, Plugins & Style CSS files) -->

    <!-- Font CSS -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600&amp;display=swap" rel="stylesheet">

    <!-- Vendor CSS (Bootstrap & Icon Font) -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/vendor/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/vendor/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/vendor/dlicon.css')}}">
    <!-- Plugins CSS (All Plugins Files) -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins/swiper-bundle.min.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins/aos.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins/odometer-theme-default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins/ddbeforeandafter.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/plugins/magnific-popup.css')}}">

    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css')}}">

</head>

<body>
    <header class="header-area header-transparent-bar sticky-bar">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 col-4">
                    <div class="logo">
                        <a href="/">
                            <img class="white-logo" src="{{ asset('frontend/assets/images/logo/logo.png')}}" alt="logo">
                            <img class="black-logo" src="{{ asset('frontend/assets/images/logo/logo-black.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-lg-block d-flex justify-content-center">
                    <div class="main-menu text-center">
                        <nav>
                            <ul>
                                @foreach ($menus as $item)
                                <li><a href="{{route('pages',$slug=$item->slug)}}">{{$item->page_name}}</a></li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-8">
                    <div class="header-action-wrap">
                        <div class="header-action-style">
                            <a class="header-aside-button" href="#"><i class="dlicon ui-3_menu-left"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- aside start -->
    <div class="header-aside-active">
        <div class="header-aside-wrap">
            <a class="aside-close"><i class="dlicon ui-1_simple-remove"></i></a>
            <div class="header-aside-content">
                <div class="mobile-menu-area">
                    <div class="mobile-menu-wrap">
                        <!-- mobile menu start -->
                        <div class="mobile-navigation">
                            <!-- mobile menu navigation start -->
                            <nav>
                                <ul class="mobile-menu">
                                    @foreach ($menus as $item)
                                    <li><a href="{{route('pages',$slug=$item->slug)}}">{{$item->page_name}}</a></li>
                                    @endforeach
                                    </ul>
                            </nav>
                            <!-- mobile menu navigation end -->
                        </div>
                        <!-- mobile menu end -->
                    </div>
                </div>
                <div class="header-aside-menu">
                    <nav>
                        <ul>
                            @foreach ($menus as $item)
                                <li><a href="{{route('pages',$slug=$item->slug)}}">{{$item->page_name}}</a></li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
                <div class="aside-contact-info">
                    <ul>
                        <li><i class="dlicon ui-1_email-84"></i>info@targetdesignsolutions.com</li>
                        <li><i class="dlicon tech-2_rotate"></i>+91 9895482013</li>
                        <li><i class="dlicon ui-1_home-minimal"></i>
                            36/546-k, 2 nd floor, manath ayyath building, manath ayyath moola road, near chithranjali studio, kakkanad- 682021
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
    <!-- Footer area -->
    <footer class="section bg-black">
        <div class="footer-top section-padding">
            <div class="container">
                <div class="row row-cols-lg-4 row-cols-md-2 row-cols-sm-2 row-cols-1 mb-n8">
                    <div class="col mb-8">
                        <div class="footer-widget footer-about">
                            <div class="footer-logo">
                                <a href="/"><img src="{{ asset('frontend/assets/images/logo/logo.png')}}" alt="logo"></a>
                            </div>
                            <p>Target Design Solutions provides customizable design services for all your needs. </p>
                            <div class="social-icon-style">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-whatsapp"></i></a>
                                <a class="google-plus" href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-8">
                        <div class="footer-widget footer-list">
                            <h3 class="footer-title">Useful Link</h3>
                            <ul>
                                @foreach ($menus as $item)
                                <li><a href="{{route('pages',$slug=$item->slug)}}">{{$item->page_name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col mb-8">
                        <div class="footer-widget footer-list">
                            <h3 class="footer-title">Contact Us</h3>
                            <ul>
                                <li>
                                    <span class="title">T:</span>
                                    <span class="desc">+91 9895482013</span>
                                </li>
                                <li>
                                    <span class="title">E:</span>
                                    <span class="desc">info@targetdesignsolutions.com</span>
                                </li>
                                <li>
                                    <span class="title">A:</span>
                                    <span class="desc">
                                        36/546-k, 2 nd floor, manath ayyath building, manath ayyath moola road, near chithranjali studio, kakkanad- 682021
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col mb-8">
                        <div class="footer-widget footer-newsletter">
                            <h3 class="footer-title">Newsletter</h3>
                            <p>Get E-mail updates about our latest newa and special offers.</p>
                            <div id="mc_embed_signup" class="subscribe-form">
                                <form id="mc-embedded-subscribe-form" class="validate" novalidate="" target="_blank" name="mc-embedded-subscribe-form" method="post" action="https://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef">
                                    <div id="mc_embed_signup_scroll" class="mc-form">
                                        <input class="email" type="email" required="" placeholder="Enter your email here.." name="EMAIL" value="">
                                        <div class="mc-news" aria-hidden="true">
                                            <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                                        </div>
                                        <div class="clear">
                                            <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="Subscribe">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="copyright text-center">
                    <p>
                        Copyright ©2021 All rights reserved | Designed by <a href="https://bijithbaby.xyz"> Coder Cap </a>.
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- JS Vendor, Plugins & Activation Script Files -->

    <!-- Vendors JS -->
    <script src="{{ asset('frontend/assets/js/vendor/modernizr-3.11.7.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/jquery-migrate-3.3.2.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/bootstrap.bundle.min.js')}}"></script>

    <!-- Plugins JS -->
    <script src="{{ asset('frontend/assets/js/plugins/swiper-bundle.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/hoverparallax.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/scrollup.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/odometer.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/ddbeforeandafter.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/magnific-popup.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/imagesloaded.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/isotope.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/aos.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/sticky-sidebar.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/plugins/ajax-mail.js')}}"></script>

    <!-- Activation JS -->
    <script src="{{ asset('frontend/assets/js/main.js')}}"></script>
    <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+916238426257", // WhatsApp number
                call: "+916238426257", // Call phone number
                call_color: "#A8CE50", // Call button color
                call_to_action: "Call Us", // Call to action
                button_color: "#FF6550", // Color of button
                position: "left", // Position may be 'right' or 'left'
                order: "call,whatsapp", // Order of buttons
            };
            var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>

</body>
</html>
