@extends('frontend.base')
@section('content')
    <div class="breadcrumb-area breadcrumb-padding bg-img" style="background-image:url(/frontend/assets/images/bg/bg-2.jpg)">
        <div class="container">
            <div class="breadcrumb-content text-center">
                <h2>Services</h2>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Services</li>
                </ul>
            </div>
        </div>
    </div>
        <!-- Services area -->
        <div class="section section-padding pt-0">
            <div class="container">
                <div class="section-title text-center mb-lg-9 mb-md-7 mt-5 mb-5">
                    <p>
                        Our design consultants enjoy the freedom of creativity and the specialized moxie to take your design from a dream to a reality. Clients often turn to us for help in finding the perfect mix of old-world luxury and new-world cool.
                    </p>
                    <hr>
                </div>
                <div class="row  mb-n6">
                    <div class="col-md-6 mb-6">
                        <div class="service text-center">
                            <img src="/frontend/assets/images/service/interior.jpg" alt="banner">
                            <div class="service-content-wrap">
                                <div class="service-content">
                                    <br>
                                    <div class="service-icon">
                                        <i class="dlicon furniture_light"></i>
                                    </div>
                                    <h3 class="title"><a href="service-details.html">Interior Design</a></h3>
                                    <p>
                                        Target Design Solutions is a multi-service interior design firm setting its own bar out of the ordinary. Whether you're a business looking to revitalize a space or you're a homeowner in need of a renovation, our design service seeks to maximize functionality and perfection. Our skilled team strives to exceed expectations and our signature will help your space become the best it can be.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <div class="service text-center">
                            <img src="/frontend/assets/images/service/exterior.jpg" alt="banner">
                            <div class="service-content-wrap">
                                <div class="service-content">
                                    <br>
                                    <div class="service-icon">
                                        <i class="dlicon design_design"></i>
                                    </div>
                                    <h3 class="title"><a href="service-details.html">Exterior Design</a></h3>
                                    <p>Our team is experienced in this field and has taken on several notable projects. We understand it's extra stressful when a deadline looms and you're unsure as to how it will turn out. For this reason, we stand by our word and our work with a 100% satisfaction guarantee. If you're not happy, we're not happy.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Working Process area -->
        <div class="section section-padding bg-img overly-style-1 opacity-point-7" style="background-image:url(/frontend/assets/images/bg/bg-1.jpg)">
            <div class="container">
                <div class="section-title text-center mb-lg-9 mb-md-7 mb-5">
                    <h2 class="title white">Our Working<span> Process</span></h2>
                    <p class="white">We're always willing to go the redundant afar, to make your dream home a reality, through a perfect mix of creativity and specialized moxie. </p>
                </div>
                <div class="row row-cols-lg-4 row-cols-md-2 row-cols-sm-2 row-cols-1 mb-n6">
                    <div class="col mb-6">
                        <div class="working-process text-center">
                            <i class="dlicon users-2_b-meeting"></i>
                            <h4 class="title">Meet The Client</h4>
                        </div>
                    </div>
                    <div class="col mb-6">
                        <div class="working-process text-center">
                            <i class="dlicon business_bulb-62"></i>
                            <h4 class="title">Make Ideas</h4>
                        </div>
                    </div>
                    <div class="col mb-6">
                        <div class="working-process text-center">
                            <i class="dlicon business_hierarchy-54"></i>
                            <h4 class="title">Development</h4>
                        </div>
                    </div>
                    <div class="col mb-6">
                        <div class="working-process text-center">
                            <i class="dlicon business_building"></i>
                            <h4 class="title">Test & Release</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
