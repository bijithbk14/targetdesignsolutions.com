@extends('frontend.base')
@section('content')
    <div class="breadcrumb-area breadcrumb-padding bg-img" style="background-image:url(/frontend/assets/images/bg/bg-2.jpg)">
        <div class="container">
            <div class="breadcrumb-content text-center">
                <h2>Gallery</h2>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Gallery</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End banner -->
    <div class="section section-padding">
        <div class="container">
            <div class="project-menu-style project-menu-active isotope-btn-add-active mb-6">
                <button class="active btn-project" data-filter="*">All</button>
                @foreach ($gallery_category as $item)
                <button class="btn-project" data-filter=".{{ \Str::slug($item) }}">{{ $item }}</button>
                @endforeach
            </div>
            <div class="row row-cols-lg-3 row-cols-md-2 row-cols-sm-2 row-cols-1 mb-n6 grid">
                @foreach ($gallery as $item)
                <div class="col mb-6 grid-item {{\Str::slug($item->category_data->category)}}">
                    <div class="project-wrap">
                        <div class="project-inner">
                            <a class="thumbnail" href="{{ $item->image }}">
                                <img src="{{ $item->image }}" alt="{{ $item->title }}">
                            </a>
                            <div class="project-content">
                                <span class="category"><a href="#">{{$item->category_data->category}}</a></span>
                                <h3 class="title"><a href="{{ $item->image }}">{{ $item->description }}</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>


@section('scripts')

@endsection
@endsection
