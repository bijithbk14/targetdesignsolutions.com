<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\MainController::class, 'index'])->name('index');
Route::get('pages/{slug}', [App\Http\Controllers\MainController::class, 'pages'])->name('pages');
Route::post('send-enquery', [App\Http\Controllers\MainController::class, 'save_contact_enq'])->name('save_contact_enq');
Route::get('service-details/{id}', [App\Http\Controllers\MainController::class, 'service_details'])->name('service_details');

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
Route::get('admin', 'App\Http\Controllers\Admin\AdminController@index')->name('home');
Route::resource('admin/roles', 'App\Http\Controllers\Admin\RolesController');
Route::resource('admin/permissions', 'App\Http\Controllers\Admin\PermissionsController');
Route::resource('admin/users', 'App\Http\Controllers\Admin\UsersController');
Route::resource('admin/activitylogs', 'App\Http\Controllers\Admin\ActivityLogsController')->only([
    'index', 'show', 'destroy'
]);
Route::resource('admin/settings', 'App\Http\Controllers\Admin\SettingsController');
Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

Route::resource('admin/delete-config', 'App\Http\Controllers\Admin\DeleteController');


#editor
Route::get('admin/admin-editor', [App\Http\Controllers\Admin\EditorController::class, 'editor'])->name('editor');
Route::get('admin/admin-edit-page/{id}/', [App\Http\Controllers\Admin\EditorController::class, 'editor_single_page'])->name('editor_single_page');
Route::post('admin/save-page', [App\Http\Controllers\Admin\EditorController::class, 'save_page'])->name('save_page');
Route::get('admin/editor/delete-page/{id}', [App\Http\Controllers\Admin\EditorController::class, 'delete_page'])->name('delete_page');

Route::post('admin/create-page', [App\Http\Controllers\Admin\EditorController::class, 'create_page'])->name('create_page');
Route::post('admin/upload-image', [App\Http\Controllers\Admin\EditorController::class, 'upload_image'])->name('upload_image');
Route::get('admin/scan-images-gallery', [App\Http\Controllers\Admin\EditorController::class, 'scan_image_gallery_cloud'])->name('scan_image_gallery_cloud');
#editor end


Route::resource('admin/pages', 'App\Http\Controllers\Admin\PagesController');

Route::get('admin/gallery/delete-image/', [App\Http\Controllers\Admin\GalleryController::class,'delete_gallery_image'])->name('remove_gallery_image');
Route::resource('admin/gallery', 'App\Http\Controllers\Admin\GalleryController');

Route::resource('admin/contactus', 'App\Http\Controllers\Admin\ContactusController');

Route::resource('admin/services', 'App\Http\Controllers\Admin\ServicesController');
Route::get('admin/service/delete-image/', [App\Http\Controllers\Admin\ServicesController::class,'delete_service_image'])->name('remove_service_image');

Route::resource('admin/category', 'App\Http\Controllers\Admin\CategoryController');
Route::resource('admin/service-details', 'App\Http\Controllers\Admin\ServiceDetailsController');
Route::get('admin/service-details/delete-image/', [App\Http\Controllers\Admin\ServicesController::class,'delete_service_details_image'])->name('remove_service_details_image');

Route::resource('admin/testimonial', 'App\Http\Controllers\Admin\TestimonialController');
});

