<?php

namespace App\Http\Controllers;

use App\Models\Contactu;
use App\Models\Gallery;
use App\Models\Service;
use App\Models\Page;
use App\Models\ServiceDetail;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function __construct(Request $request)
    {
        $meta_data['title'] = '';
        $meta_data['url'] = $request->url();
        $meta_data['description'] = '';
        $meta_data['image'] = '';
        $meta_data['keywords'] = '';
        View::share('meta_data', $meta_data);
        $menu = Page::orderBy('order', 'asc')->get();
        View::share('menus', $menu);
    }
    public function index(Request $request)
    {
        $page = Page::where('slug', 'home')->first();
        $file_name = str_replace('.blade.php', '', $page->file_name);
        $testimonial = Testimonial::all()->toArray();
        $gallery =  Gallery::all();
        $gallery_category = array();
        foreach ($gallery as $item) {
            if (!in_array($item->category_data->category, $gallery_category)) {
                array_push($gallery_category, $item->category_data->category);
            }
        }
        return view('frontend.' . $page->folder . '.' . $file_name, compact('page','gallery_category','gallery', 'testimonial'));
    }

    public function pages(Request $request)
    {
        $gallery = array();
        $services = array();
        $gallery_category = array();
        $page = Page::where('slug', $request->slug)->first();
        $file_name = str_replace('.blade.php', '', $page->file_name);
        $gallery =  Gallery::all();
        foreach ($gallery as $item) {
            if (!in_array($item->category_data->category, $gallery_category)) {
                array_push($gallery_category, $item->category_data->category);
            }
        }
        if ($page->slug == 'services') {
            $services =  Service::all()->toArray();
        }
        $testimonial = Testimonial::all()->toArray();
        if ($page->folder != "") {
            $data = array(
                'pages' => 'test',
                'gallery' => $gallery,
                'gallery_category' => $gallery_category,
                'services' => $services,
                'page' => $page,
                'testimonial' => $testimonial
            );
            return view('frontend.' . $page->folder . '.' . $file_name)->with($data);
        } else {
            $pages = Page::all()->toArray();
            $data = array(
                'pages' => $pages,
                'gallery' => $gallery,
                'gallery_category' => $gallery_category,
                'services' => $services,
                'page' => $page,
                'testimonial' => $testimonial
            );
            return view('frontend.' . $page->folder . '.' . $file_name)->with($data);
        }
    }

    public function service_details(Request $request)
    {
        $gallery_category = array();
        $service = Service::where('id', $request->id)->first();
        $details = ServiceDetail::where('service', $request->id)->get();
        $page = $service;
        foreach ($details as $item) {
            if (!in_array($item->title, $gallery_category)) {
                array_push($gallery_category, $item->title);
            }
        }
        return view('frontend.service-details', compact('service', 'details', 'page', 'gallery_category'));
    }

    function save_contact_enq(Request $request)
    {

        $contact = new Contactu;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();
        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'subject' => $request->subject,
            'message' => $request->message,
        ];
        // Mail::to('info@tribunets.com')->send(new \App\Mail\ContactForm($details));
        $data = array('result' => 'success', 'message' => 'Thankyou for contacting us!');
        return json_encode($data);
    }
}
