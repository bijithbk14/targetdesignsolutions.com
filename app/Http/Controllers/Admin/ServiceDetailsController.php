<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Service;
use App\Models\ServiceDetail;
use Illuminate\Http\Request;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
class ServiceDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $servicedetails = ServiceDetail::where('service', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $servicedetails = ServiceDetail::latest()->paginate($perPage);
        }

        return view('admin.service-details.index', compact('servicedetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $services = Service::all();
        return view('admin.service-details.create',compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'service' => 'required',
			'title' => 'required',
			'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:500',
		]);
        $requestData = $request->all();
        if($request->image){
            $image = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
            $requestData['image'] = $image;
        }
        ServiceDetail::create($requestData);

        return redirect('admin/service-details')->with('flash_message', 'ServiceDetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $servicedetail = ServiceDetail::findOrFail($id);

        return view('admin.service-details.show', compact('servicedetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $servicedetail = ServiceDetail::findOrFail($id);
        $services = Service::all();
        return view('admin.service-details.edit', compact('servicedetail','services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'service' => 'required',
			'title' => 'required',
		]);
        $requestData = $request->all();
        if($request->image){
            $image = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
            $requestData['image'] = $image;
        }
        $servicedetail = ServiceDetail::findOrFail($id);
        $servicedetail->update($requestData);

        return redirect('admin/service-details')->with('flash_message', 'ServiceDetail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ServiceDetail::destroy($id);

        return redirect('admin/service-details')->with('flash_message', 'ServiceDetail deleted!');
    }

    public function delete_service_details_image(Request $request){
        $course = ServiceDetail::findOrFail($request->id);
        $course->update(array('image'=>''));
        return redirect()->back();
    }
}
