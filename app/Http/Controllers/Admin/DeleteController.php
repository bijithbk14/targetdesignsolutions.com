<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Schema;
use Exception;

class DeleteController extends Controller
{
    public function index(Request $request)
    {
        $json_file = json_decode(File::get(base_path('resources/laravel-admin/menus.json')), true);
        $json_data = (array) json_decode(File::get(base_path('resources/laravel-admin/menus.json')));
        $menus = (array)$json_data['menus'][0];
        $menus = $menus['items'];

        if (isset($request->name)) {
            $protected = array("Activity Logs", "Users", "Roles", "Permissions", "Settings");
            if (in_array($request->name, $protected)) {
                return redirect('admin/delete-config')->with('flash_message', 'Cant Delete, Item Protected');
            } else {
                // /var/www/html/laravel/crud-generator/crud-admin/app/Http/Controllers/Admin/DeleteController.php
                // print(base_path());
                $controllerName = $request->name . 'Controller';
                $controllerPath = base_path('app/Http/Controllers/Admin/' . $controllerName . '.php');
                $modelName = $request->name;
                if (substr($modelName, -1) == 's') {
                    $modelName = substr_replace($modelName, "", -1);
                }
                $modelPath = base_path('app/Models/' . $modelName . '.php');
                $viewName = strtolower($request->name);
                $viewPath = base_path('resources/views/admin/' . $viewName);
                $migrationDirectory = glob(base_path('database/migrations/*')); // get all file names
                $migrationFileName = 'create_' . $viewName;
                $viewDirectory = glob($viewPath . '/*');
                //delete files like controller models views etc here.
                try {

                    if (is_file($controllerPath)) {
                        unlink($controllerPath); // delete file
                    }

                    if (is_file($modelPath)) {
                        unlink($modelPath); // delete file
                    }
                    if (is_dir($viewPath)) {
                        foreach ($viewDirectory as $file) {
                            if (is_file($file)) {
                                unlink($file); // delete file
                            }
                        }
                        rmdir($viewPath); // delete directory
                    }
                    //delete migration files
                    foreach ($migrationDirectory as $file) { // iterate files
                        // echo $file;
                        if (strpos($file, $migrationFileName) !== false) {
                            if (is_file($file)) {
                                unlink($file); // delete file
                            }
                        }
                    }
                    //drop table
                    if (substr($viewName, -1) == 's') {
                        $table = $viewName;
                        Schema::dropIfExists($table);
                    } else {
                        $table = $viewName . 's';
                        Schema::dropIfExists($table);
                    }

                    //recreate menu.json
                    $data = array();
                    for ($i = 0; $i < count($json_file); $i++) {
                        $k = 0;
                        foreach ($json_file['menus'][$i]['items'] as $row) {
                            if ($row['title'] == $request->name) {
                                unset($json_file['menus'][$i]['items'][$k]);
                            } else {
                                array_push($data, (object)['title' => $row['title'], 'url' => $row['url']]);
                            }
                            $k++;
                        }
                        $json_file['menus'][$i]['items'] = $data;
                    }
                    // print_r($x);
                    File::put(base_path('resources/laravel-admin/menus.json'), json_encode($json_file));

                    $matchingLine = 'admin/' . $viewName;
                    $rows = file(base_path('routes/web.php'));
                    foreach ($rows as $key => $row) {
                        if (strpos($row, $matchingLine) !== false) {
                            unset($rows[$key]);
                        }
                    }
                    File::put(base_path('routes/web.php'), $rows);

                    // file_put_contents("solved.txt", implode("\n", $rows));

                } catch (Exception $e) {
                    return redirect('admin/delete-config')->with('flash_message', $e);
                }
                return redirect('admin/delete-config')->with('flash_message', 'Stuffs Deleted');
                // print($controllerPath);
            }
        }
        return view('admin.delete.index', compact('menus'));
    }
}
