<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use App\Models\Cloudinary as CloudinaryTable;
use App\Models\Page;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class EditorController extends Controller
{
    public function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 90)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch ($mime) {
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if ($width_new > $width) {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if ($dst_img) imagedestroy($dst_img);
        if ($src_img) imagedestroy($src_img);
    }
    public function scan_image_gallery_cloud(Request $request)
    {
        $cloudinary_images = CloudinaryTable::all();
        // print_r( $cloudinary_images);
        if ($cloudinary_images) {
            $files = [];
            foreach ($cloudinary_images as $images) {
                $files[] = [
                    'path'  => $images->image_url,
                ];
            }
            header('Content-type: application/json');
            echo json_encode([
                'items' => $files,
            ]);
        }
    }
    public function upload_image(Request $request)
    {

        if ($request->method() == 'POST') {
            $request->validate([
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:500',
            ]);
            $original = Cloudinary::upload($request->file('file')->getRealPath())->getSecurePath();
            // $resizedImage = cloudinary()->upload($request->file('file')->getRealPath(), [
            //     'folder' => 'uploads',
            //     'transformation' => [
            //         'width' => 850,
            //         'height' => 650,
            //         'crop' => 'fill'
            //     ]
            // ])->getSecurePath();

            if ($original) {
                $obj = new CloudinaryTable;
                $obj->image_url = $original;
                $obj->save();
                return $original;
            } else {
                return 'error';
            }
        }
    }

    public function editor()
    {
        $pages = Page::where('is_edit',1)->get();
        $file_name = '';
        return view('admin.editor', compact('pages', 'file_name'));
    }

    public function editor_single_page(Request $request)
    {
        $page = Page::where('id', $request->id)->where('is_edit',1)->first();
        if ($page->folder != "") {
            $path = base_path('resources/views/frontend') . '/' . $page->folder . '/' . $page->file_name;
        } else {
            $path = base_path('resources/views/frontend') . '/' . $page->file_name;
        }
        $rewrite_file = File::get($path);
        $pattern = preg_quote('@{{', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('{{', '@{{', $rewrite_file);
        }
        $pattern = preg_quote('@@foreach', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('@foreach', '@@foreach', $rewrite_file);
        }
        $pattern = preg_quote('@@endforeach', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('@endforeach', '@@endforeach', $rewrite_file);
        }

        $pattern = preg_quote('@@if', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('@if', '@@if', $rewrite_file);
        }
        $pattern = preg_quote('@@endif', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('@endif', '@@endif', $rewrite_file);
        }
        $pattern = preg_quote('@@else', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('@else', '@@else', $rewrite_file);
        }
        $pattern = preg_quote('@@elseif', '/');
        $pattern = "/^.*$pattern.*\$/m";
        if(!preg_match_all($pattern, $rewrite_file, $matches)){
            $rewrite_file = str_replace('@elseif', '@@elseif', $rewrite_file);
        }
        File::put($path, $rewrite_file);
        $file_name = 'pages/' . $page->slug;
        $pages = Page::where('is_edit',1)->get();
        return view('admin.editor', compact('page', 'pages', 'file_name'));
    }

    public function save_page(Request $request)
    {

        $page = Page::where('id', $request->page_id)->first();
        $file_name = $page->file_name;
        $folder = $page->folder;
        $file_content = "@extends('frontend.base')\n";
        $file_content .= "@section('content')\n";
        $file_content .= $request->html;
        $file_content .= "\n@endsection";
        if ($folder != "") {
            $path = base_path('resources/views/frontend') . '/' . $folder . '/' . $file_name;
        } else {
            $path = base_path('resources/views/frontend') . '/' . $file_name;
        }
        File::put($path, $file_content);
        return 'Successfully Saved';
    }

    public function create_page(Request $request)
    {
        $page = new Page;
        $page->template = $request->template;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->meta_keywords = $request->meta_keywords;
        $page->page_name = $request->page_name;
        $page->file_name = $request->file_name;
        $page->folder = $request->folder;
        $page->slug = Str::slug($request->page_name);
        $page->save();

        $file_name = $request->file_name;
        $folder = $request->folder;
        $folder_path = base_path('resources/views/frontend') . '/' . $folder;
        if ($folder != "") {
            File::makeDirectory($folder_path, $mode = 0777, true, true);
            $path = base_path('resources/views/frontend') . '/' . $folder . '/' . $file_name;
        } else {
            $path = base_path('resources/views/frontend') . '/' . $file_name;
        }

        $file_content = File::get(base_path('resources/frontend-generator/blankpage.blade.stub'));
        File::put($path, $file_content);

        return redirect()->back();
    }

    public function delete_page(Request $request)
    {
        $page = Page::where('id', $request->id);
        $page = $page->first();
        if ($page->folder != "") {
            $path = base_path('resources/views/frontend') . '/' . $page->folder . '/' . $page->file_name;
        } else {
            $path = base_path('resources/views/frontend') . '/' . $page->file_name;
        }
        if (is_file($path)) {
            unlink($path); // delete file
        }
        $page->delete();
        return redirect()->back();
    }
}
