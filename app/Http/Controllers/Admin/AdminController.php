<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Page;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 90)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch ($mime) {
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if ($width_new > $width) {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if ($dst_img) imagedestroy($dst_img);
        if ($src_img) imagedestroy($src_img);
    }

    public function gallery(Request $request)
    {


        if ($request->method() == 'POST') {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:500',
            ]);
            $original = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
            $resizedImage = cloudinary()->upload($request->file('image')->getRealPath(), [
                'folder' => 'uploads',
                'transformation' => [
                    'width' => 850,
                    'height' => 650,
                    'crop' => 'fill'
                ]
            ])->getSecurePath();
            // $path = $request->file('image')->getRealPath();
            // $image_original = file_get_contents($path);
            // $original_base64 = base64_encode($image_original);
            // // echo '<img src="data:image/jpg;base64,'.$original_base64.'">';
            // $imageName = time() . '.' . $request->image->extension();
            // $request->image->move(public_path('gallery'), $imageName);
            // $original = public_path('gallery').'/'.$imageName;
            // $thumb = public_path('gallery').'/thumb_'.$imageName;
            // $this->resize_crop_image('850','650',$original,$thumb);
            // $thumb_base64 = base64_encode(file_get_contents($thumb));
            // if (file_exists($original)) {
            //     unlink($original);
            //     unlink($thumb);
            // }
            $gallery = new Gallery();
            $gallery->title = $request->title;
            $gallery->image = $original;
            $gallery->thumb = $resizedImage;
            $gallery->sort = 0;
            $gallery->save();

            return back()->with('success', 'You have successfully upload image.');
        } else {
            $gallery = Gallery::all();
            return view('admin.gallery', compact('gallery'));
        }
    }



    // public function editor_single_page(Request $request)
    // {
    //     $page = Page::where('id', $request->id)->first();
    //     $html_content = '<head>
    //                         <meta charset="utf-8">
    //                         <meta http-equiv="x-ua-compatible" content="ie=edge">
    //                         <title>Shamrock Apartments</title>
    //                         <meta name="robots" content="noindex, follow" />
    //                         <meta name="description" content="">
    //                         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    //                         <!-- Place favicon.png in the root directory -->
    //                         <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
    //                         <!-- Font Icons css -->
    //                         <link rel="stylesheet" href="/css/font-icons.css">
    //                         <!-- plugins css -->
    //                         <link rel="stylesheet" href="/css/plugins.css">
    //                         <!-- Main Stylesheet -->
    //                         <link rel="stylesheet" href="/css/style.css">
    //                         <!-- Responsive css -->
    //                         <link rel="stylesheet" href="/css/responsive.css">
    //                     </head><body id="content-to-save">';
    //     $html_content .= $page->body_content;
    //     $html_content .= '</body></html>';
    //     $file_name = time() . '.html';
    //     $path = public_path('pages') . '/' . $file_name;
    //     $files = glob(public_path('pages') . '/*'); // get all file names
    //     foreach ($files as $file) { // iterate files
    //         if (is_file($file)) {
    //             unlink($file); // delete file
    //         }
    //     }
    //     File::put($path, $html_content);
    //     $pages = Page::all();
    //     return view('admin.editor', compact('page', 'pages', 'file_name'));
    // }



}
