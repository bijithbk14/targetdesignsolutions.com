<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Category;
use App\Models\Gallery;
use Illuminate\Http\Request;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $gallery = gallery::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $gallery = gallery::latest()->paginate($perPage);
        }

        return view('admin.gallery.index', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.gallery.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
			'title' => 'required',
			'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:500',
		]);
        $requestData = $request->all();
        if($request->image){
            $course_image = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
            $requestData['image'] = $course_image;
        }
        gallery::create($requestData);

        return redirect('admin/gallery')->with('flash_message', 'gallery added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $gallery = gallery::findOrFail($id);

        return view('admin.gallery.show', compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $gallery = gallery::findOrFail($id);
        $category = Category::all();
        return view('admin.gallery.edit', compact('gallery','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
			'title' => 'required',
		]);
        $requestData = $request->all();
        if($request->image){
            $course_image = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
            $requestData['image'] = $course_image;
        }
        $gallery = gallery::findOrFail($id);
        $gallery->update($requestData);

        return redirect('admin/gallery')->with('flash_message', 'gallery updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        gallery::destroy($id);

        return redirect('admin/gallery')->with('flash_message', 'gallery deleted!');
    }

    public function delete_gallery_image(Request $request){
        $course = Gallery::findOrFail($request->id);
        $course->update(array('image'=>''));
        return redirect()->back();
    }
}
