-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: laravel-admin
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint unsigned DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint unsigned DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject` (`subject_type`,`subject_id`),
  KEY `causer` (`causer_type`,`causer_id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'default','App\\Models\\Page model has been created','App\\Models\\Page',1,'App\\Models\\User',1,'[]','2021-12-30 23:45:23','2021-12-30 23:45:23'),(2,'default','App\\Models\\Page model has been created','App\\Models\\Page',2,'App\\Models\\User',1,'[]','2021-12-30 23:49:25','2021-12-30 23:49:25'),(3,'default','App\\Models\\Page model has been created','App\\Models\\Page',3,'App\\Models\\User',1,'[]','2021-12-31 00:42:17','2021-12-31 00:42:17'),(4,'default','App\\Models\\Page model has been created','App\\Models\\Page',4,'App\\Models\\User',1,'[]','2021-12-31 00:43:42','2021-12-31 00:43:42'),(5,'default','App\\Models\\Page model has been created','App\\Models\\Page',5,'App\\Models\\User',1,'[]','2021-12-31 02:45:14','2021-12-31 02:45:14'),(6,'default','App\\Models\\Page model has been created','App\\Models\\Page',6,'App\\Models\\User',1,'[]','2021-12-31 03:54:32','2021-12-31 03:54:32'),(7,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',6,'App\\Models\\User',1,'[]','2022-01-01 02:00:04','2022-01-01 02:00:04'),(8,'default','App\\Models\\Page model has been created','App\\Models\\Page',7,'App\\Models\\User',1,'[]','2022-01-01 02:01:09','2022-01-01 02:01:09'),(9,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',7,'App\\Models\\User',1,'[]','2022-01-01 02:02:58','2022-01-01 02:02:58'),(10,'default','App\\Models\\Page model has been created','App\\Models\\Page',8,'App\\Models\\User',1,'[]','2022-01-01 03:05:21','2022-01-01 03:05:21'),(11,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',8,'App\\Models\\User',1,'[]','2022-01-03 23:00:19','2022-01-03 23:00:19'),(12,'default','App\\Models\\Page model has been created','App\\Models\\Page',9,'App\\Models\\User',1,'[]','2022-01-03 23:00:40','2022-01-03 23:00:40'),(13,'default','App\\Models\\Page model has been created','App\\Models\\Page',10,'App\\Models\\User',1,'[]','2022-01-04 06:17:46','2022-01-04 06:17:46'),(14,'default','App\\Models\\Page model has been created','App\\Models\\Page',11,'App\\Models\\User',1,'[]','2022-01-04 06:18:59','2022-01-04 06:18:59'),(15,'default','App\\Models\\Page model has been created','App\\Models\\Page',12,'App\\Models\\User',1,'[]','2022-01-04 06:19:22','2022-01-04 06:19:22'),(16,'default','App\\Models\\Page model has been created','App\\Models\\Page',13,'App\\Models\\User',1,'[]','2022-01-04 06:19:42','2022-01-04 06:19:42'),(17,'default','App\\Models\\Page model has been created','App\\Models\\Page',14,'App\\Models\\User',1,'[]','2022-01-05 12:33:16','2022-01-05 12:33:16'),(18,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',14,'App\\Models\\User',1,'[]','2022-01-05 12:34:29','2022-01-05 12:34:29'),(19,'default','App\\Models\\Cloudinary model has been created','App\\Models\\Cloudinary',6,'App\\Models\\User',1,'[]','2022-01-08 04:30:56','2022-01-08 04:30:56'),(20,'default','App\\Models\\Cloudinary model has been created','App\\Models\\Cloudinary',7,'App\\Models\\User',1,'[]','2022-01-08 06:08:24','2022-01-08 06:08:24'),(21,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',1,'App\\Models\\User',3,'[]','2022-01-22 20:37:04','2022-01-22 20:37:04'),(22,'default','App\\Models\\Gallery model has been updated','App\\Models\\Gallery',1,'App\\Models\\User',3,'[]','2022-01-22 20:40:38','2022-01-22 20:40:38'),(23,'default','App\\Models\\Gallery model has been updated','App\\Models\\Gallery',1,'App\\Models\\User',3,'[]','2022-01-22 20:41:57','2022-01-22 20:41:57'),(24,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',2,'App\\Models\\User',3,'[]','2022-01-22 20:56:01','2022-01-22 20:56:01'),(25,'default','App\\Models\\Gallery model has been updated','App\\Models\\Gallery',1,'App\\Models\\User',3,'[]','2022-01-22 21:02:29','2022-01-22 21:02:29'),(26,'default','App\\Models\\Gallery model has been updated','App\\Models\\Gallery',1,'App\\Models\\User',3,'[]','2022-01-22 21:03:02','2022-01-22 21:03:02'),(27,'default','App\\Models\\Page model has been updated','App\\Models\\Page',12,'App\\Models\\User',3,'[]','2022-01-22 21:05:58','2022-01-22 21:05:58'),(28,'default','App\\Models\\Page model has been created','App\\Models\\Page',15,'App\\Models\\User',3,'[]','2022-01-22 21:39:56','2022-01-22 21:39:56'),(29,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',15,'App\\Models\\User',3,'[]','2022-01-22 21:41:51','2022-01-22 21:41:51'),(30,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',3,'App\\Models\\User',3,'[]','2022-01-22 21:57:42','2022-01-22 21:57:42'),(31,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',4,'App\\Models\\User',3,'[]','2022-01-22 21:58:13','2022-01-22 21:58:13'),(32,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',5,'App\\Models\\User',3,'[]','2022-01-22 21:58:51','2022-01-22 21:58:51'),(33,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',6,'App\\Models\\User',3,'[]','2022-01-22 21:59:49','2022-01-22 21:59:49');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cloudinaries`
--

DROP TABLE IF EXISTS `cloudinaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cloudinaries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cloudinaries`
--

LOCK TABLES `cloudinaries` WRITE;
/*!40000 ALTER TABLE `cloudinaries` DISABLE KEYS */;
INSERT INTO `cloudinaries` VALUES (1,'2021-12-11 00:32:45','2021-12-11 00:32:45','https://res.cloudinary.com/dxxlsebas/image/upload/v1639202564/l2rbuktnilfmxtjrmvim.png'),(2,'2021-12-11 00:45:30','2021-12-11 00:45:30','https://res.cloudinary.com/dxxlsebas/image/upload/v1639203330/ia7jy1snioq3e4ermkxb.png'),(3,'2021-12-11 01:10:04','2021-12-11 01:10:04','https://res.cloudinary.com/dxxlsebas/image/upload/v1639204803/omvwkeh99hvgvnye4wgd.png'),(4,'2021-12-30 07:09:40','2021-12-30 07:09:40','https://res.cloudinary.com/dxxlsebas/image/upload/v1640867980/uoolwbjfrl0klbasffva.png'),(5,'2021-12-30 07:10:46','2021-12-30 07:10:46','https://res.cloudinary.com/dxxlsebas/image/upload/v1640868046/lludrk4zc2cepzwaq3sh.png'),(6,'2022-01-08 04:30:56','2022-01-08 04:30:56','https://res.cloudinary.com/dxxlsebas/image/upload/v1641636055/v2vf9h9v3nrrwiw5y9jh.jpg'),(7,'2022-01-08 06:08:24','2022-01-08 06:08:24','https://res.cloudinary.com/dxxlsebas/image/upload/v1641641903/cg4fyv5bzsgt2ydkxnd9.jpg');
/*!40000 ALTER TABLE `cloudinaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galleries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'2022-01-22 20:37:04','2022-01-22 21:03:02','Abu Dhabi','https://res.cloudinary.com/dxxlsebas/image/upload/v1642905182/v01whgqaflgskvixjcro.jpg','Wooden bridge work including main steel structure'),(2,'2022-01-22 20:56:01','2022-01-22 20:56:01','Dubai','https://res.cloudinary.com/dxxlsebas/image/upload/v1642904760/d03vgxvrh1ukjfklpma6.jpg','Cabana / Pergola work in Private farm'),(3,'2022-01-22 21:57:42','2022-01-22 21:57:42','Wooden works','https://res.cloudinary.com/dxxlsebas/image/upload/v1642908462/ivwgr7wxvzzspkkmcxlj.jpg',NULL),(4,'2022-01-22 21:58:13','2022-01-22 21:58:13','Steel Fabrication / Car Parking','https://res.cloudinary.com/dxxlsebas/image/upload/v1642908492/uc1xevsnbsfctafatwt2.jpg',NULL),(5,'2022-01-22 21:58:51','2022-01-22 21:58:51','Aluminum Fabrication','https://res.cloudinary.com/dxxlsebas/image/upload/v1642908531/m7zn7xxklslshsmlt8sa.jpg',NULL),(6,'2022-01-22 21:59:49','2022-01-22 21:59:49','Wooden Flooring','https://res.cloudinary.com/dxxlsebas/image/upload/v1642908588/kaaehfzf7oyibtczjdju.jpg',NULL);
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (6,'2018_08_01_183154_create_pages_table',2),(8,'2021_12_10_123104_create_activity_log_table',2),(9,'2021_12_10_125003_create_posts_table',3),(10,'2021_12_10_142808_create_tests_table',4),(11,'2021_12_10_144459_create_pages_table',5),(12,'2021_12_10_150003_create_tests_table',6),(13,'2021_12_10_153308_create_posts_table',7),(14,'2021_12_11_055558_create_cloudinaries_table',8),(15,'2021_12_11_070215_create_tests_table',9),(16,'2021_12_11_070320_create_anothers_table',10),(23,'2014_10_12_000000_create_users_table',11),(24,'2014_10_12_100000_create_password_resets_table',11),(25,'2016_01_01_193651_create_roles_permissions_tables',11),(26,'2018_08_04_122319_create_settings_table',11),(27,'2019_08_19_000000_create_failed_jobs_table',11),(28,'2019_12_14_000001_create_personal_access_tokens_table',11),(29,'2021_12_10_121031_create_activity_log_table',11),(30,'2021_12_31_042153_create_pages_table',11),(31,'2022_01_08_122629_create_galleries_table',12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template` int DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (9,'2022-01-03 23:00:40','2022-01-03 23:00:40',1,NULL,'home','home','Home','home.blade.php',NULL,'home',1),(10,'2022-01-04 06:17:46','2022-01-04 06:17:46',1,NULL,'About Us','About Us','About Us','about-us.blade.php',NULL,'about-us',2),(11,'2022-01-04 06:18:59','2022-01-04 06:18:59',1,NULL,'Services','Services','Services','services.blade.php',NULL,'services',3),(12,'2022-01-04 06:19:22','2022-01-22 21:05:58',1,'Gallery','Gallery','Gallery','Gallery','gallery.blade.php',NULL,'gallery',4),(13,'2022-01-04 06:19:42','2022-01-04 06:19:42',1,NULL,'Contact Us','Contact Us','Contact Us','contact-us.blade.php',NULL,'contact-us',5);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user` (
  `role_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'bijith','bijith@gmail.com',NULL,'$2y$10$26HB08yK6y0Qv0su3s9YKujfOkyPbR8N9S4pgCzEp3W8ks3GV8gni',NULL,'2021-12-30 22:56:12','2021-12-30 22:56:12'),(2,'Admin','admin@admin.com',NULL,'$2y$10$zO2YrJx/pyvjjsrSreMZ9.I8l.vm5otuzd1mbLgh41B/n70RGVua2',NULL,'2022-01-15 21:55:52','2022-01-15 21:55:52'),(3,'Admin','admin@2022.com',NULL,'$2y$10$XYB/xmFk3rNHq0VjMJ1CveOGy8nadffsgtmG36KOb8mWuIHoJxs3e',NULL,'2022-01-22 20:35:58','2022-01-22 20:35:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-23  9:07:33
